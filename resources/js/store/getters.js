export default {
  post(state) {
    return state.post
  },
  comments(state) {
    return state.comments
  },
  user(state) {
    return state.user
  },
};

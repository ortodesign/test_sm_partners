// const resourceApi = 'http://test-sm-partners.local/comments'
// const resourceApi = 'http://127.0.0.1:8000/comments'
const resourceApi = process.env.MIX_URL + '/comments'
console.log(resourceApi)
export default {
  async getComments({commit}) {
    try {
      const {data} = await axios.get(resourceApi)
      commit('getComments', data.data)
    } catch (e) {
      alert('Ошибка загрузки комментариев')
    }
  },
  async storeComment({commit, dispatch}, comment) {
    try {
      const {data} = await axios.post(resourceApi, comment)
      dispatch('getComments')
    } catch (e) {
      alert('Ошибка добавления комментария')
    }
  },
  async updateComment({commit, dispatch}, comment) {
    try {
      const {data} = await axios.put(resourceApi +`/${comment.id}`, comment)
      dispatch('getComments')
    } catch (e) {
      alert('Ошибка исправления комментария')
    }
  },
  async delComment({commit, dispatch}, comment) {
    try {
      const {data} = await axios.delete(resourceApi + `/${comment.id}`)
      dispatch('getComments')
    } catch (e) {
      alert('Ошибка удаления комментария')
    }
  },
}

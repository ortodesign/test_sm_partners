<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $response = Comment::all();
    return JsonResource::collection($response);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {

    $validated = $request->validate([
      'user_id' => 'required',
      'author' => 'required|max:255',
      'content' => 'required|unique:comments|max:4096',
    ]);
    $comment = Comment::create($validated);
    return $comment;


  }

  /**
   * Display the specified resource.
   *
   * @param \App\Models\Comment $comment
   * @return \Illuminate\Http\Response
   */
  public function show(Comment $comment)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\Models\Comment $comment
   * @return \Illuminate\Http\Response
   */
  public function edit(Comment $comment)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Comment $comment
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Comment $comment)
  {
    $validated = $request->validate([
      'id' => 'required',
      'user_id' => 'required',
      'author' => 'required|max:255',
      'content' => 'required|unique:comments|max:4096',
    ]);
    $response = Comment::query()->where('id',$comment->id)->update($validated);
    return $response;

  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Models\Comment $comment
   * @return \Illuminate\Http\Response
   */
  public function destroy(Comment $comment)
  {
    $response = Comment::where('id', $comment->id)->delete();
    return $response;
  }
}
